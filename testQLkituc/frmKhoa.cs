﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using testQLkituc.Control;
using testQLkituc.Data;

namespace testQLkituc
{
    public partial class frmKhoa : Form
    {
        public CtrlKhoa ctrlk=new CtrlKhoa();
        public frmKhoa()
        {
            InitializeComponent();
        }
          public void Showk(int a)
        {
            Khoa kh = new Khoa();
            kh = frmcha.DSCHA.Dsk[a];
            txtMakhoa.Text = kh.MaKhoa;
            txtTenKhoa.Text = kh.TenKhoa;

        }
        public void ShowListView()
        {
            lvwTTKhoa.Items.Clear();
            foreach (Khoa kh in frmcha.DSCHA.Dsk)
            {
                lvwTTKhoa.Items.Add(kh.MaKhoa);
                int j = lvwTTKhoa.Items.Count - 1;
                lvwTTKhoa.Items[j].SubItems.Add(kh.TenKhoa);
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            Lop l = new Lop();
            Khoa kh = new Khoa();
            kh.TenKhoa = txtTenKhoa.Text;
            kh.MaKhoa = txtMakhoa.Text;
            if (ctrlk.themk(kh) == true)
            {
                ShowListView();
            }
        
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
        int flag = 0;
            foreach (int item in lvwTTKhoa.SelectedIndices)
            {
                if (flag == 0)
                {
                    ctrlk.Xoak(item);

                }
                else
                    ctrlk.Xoak(item - flag);
                flag++;
            }
            ShowListView();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
        Khoa kh = new Khoa();
            kh.MaKhoa = txtMakhoa.Text;
            kh.TenKhoa = txtTenKhoa.Text;
            if (!ctrlk.suak(kh))
                MessageBox.Show("Không sửa dữ liệu được!");
            else
            {
                ShowListView();
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
         Close();
        }

        private void lvwTTKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {
        foreach (int a in lvwTTKhoa.SelectedIndices)
            {
                Showk(a);

            }
        }

        private void frmKhoa_Load(object sender, EventArgs e)
        {
        ShowListView();
           
        
        }

    }
}
