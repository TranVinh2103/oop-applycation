﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using testQLkituc.Data;
namespace testQLkituc
{
    public partial class frmcha : Form
    {
        public static Cha DSCHA = new Cha();
       // private Control.CtrlHopDong ctrlhd = new Control.CtrlHopDong();
        public frmcha()
        {
            InitializeComponent();

        }

        private void mnuFileHopDong_Click(object sender, EventArgs e)
        {
            frmHopDong frhd = new frmHopDong();
            if (frhd.ShowDialog() == DialogResult.OK)
            {

            }
        }

        private void mnuFileSinhVien_Click(object sender, EventArgs e)
        {
            frmSinhVien frsv = new frmSinhVien();
            if (frsv.ShowDialog() == DialogResult.OK)
            {

            }
        }

        private void qLPhongToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPhong frp = new frmPhong();
            if(frp.ShowDialog()==DialogResult.OK)
            {

            }
        }

        private void qLKhoaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmKhoa frk = new frmKhoa();
            frk.MdiParent = this;
            frk.Show();
        }

        private void qLLơpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLop frl = new frmLop();
            frl.ShowDialog();
        }

        private void frmcha_Load(object sender, EventArgs e)
        {
            Cha ch = new Cha();
            ch.readfile();
        }

        private void lưuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cha ch = new Cha();
            ch.LuuFile();
        }
    }
}
