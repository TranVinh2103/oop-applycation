﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testQLkituc.Data
{
    [Serializable]
    public class HopDong
    {
        private string mahd;
        private DateTime ngaybd;
        private DateTime ngaykt;
        private string tinhtrang;
        private SinhVien m_sinhvien;

        public SinhVien Sinhvien
        {
            get { return m_sinhvien; }
            set { m_sinhvien = value; }
        }

        public string Mahd
        {
            get { return mahd; }
            set { mahd = value; }
        }
       

        public DateTime Ngaybd
        {
            get { return ngaybd; }
            set { ngaybd = value; }
        }
        

        public DateTime Ngaykt
        {
            get { return ngaykt; }
            set { ngaykt = value; }
        }
       

        public string Tinhtrang
        {
            get { return tinhtrang; }
            set { tinhtrang = value; }
        }
        public HopDong()
        {
            mahd = "";
            ngaybd = DateTime.Today;
            ngaykt = DateTime.Today;
            tinhtrang = "";
        }
        public HopDong(string mhd,DateTime ngbd,DateTime ngkt,string ttrang)
        {
            mahd =mhd;
            ngaybd = ngbd;
            ngaykt = ngkt;
            tinhtrang = ttrang;
        }
    }
}
