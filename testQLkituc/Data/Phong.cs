﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testQLkituc.Data
{
    [Serializable]
    public class Phong
    {
        public string MaPhong { get; set; }
        public string TenPhong { get; set; }
        public bool LoaiPhong { get; set; }
        public float DienTich { get; set; }
        public string TinhTrang { get; set; }
        private ChiTietPhong ctPhong;

        public ChiTietPhong CTPhong
        {
            get { return ctPhong; }
            set { ctPhong = value; }
        }
        public Phong()
        {
            MaPhong = "";
            TenPhong = "";
            LoaiPhong = false;
            DienTich = 0;
            TinhTrang = "";
        }
        public Phong(string Map, string Tenp, bool Loaip, float dient, string tinhtr)
        {
            MaPhong = Map;
            TenPhong = Tenp;
            LoaiPhong = Loaip;
            DienTich = dient;
            TinhTrang = tinhtr;
        }

    }
}
