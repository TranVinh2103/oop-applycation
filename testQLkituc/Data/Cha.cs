﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace testQLkituc.Data
{
    [Serializable]
    public class Cha
    {
        private List<HopDong> dshd;
        private List<SinhVien> dssv;
        private List<Phong> dsp;
        private List<Lop> dsl;
        private List<Khoa> dsk;

        public Cha()
        {
            dshd = new List<HopDong>();
            dssv = new List<SinhVien>();
            dsp = new List<Phong>();
            dsl = new List<Lop>();
            dsk = new List<Khoa>();
        }
        public List<Phong> Dsp
        {
            get { return dsp; }
            set { dsp = value; }
        }
        public List<HopDong> Dshd
        {
            get { return dshd; }
            set { dshd = value; }
        }
        public List<SinhVien> Dssv
        {
            get { return dssv; }
            set { dssv = value; }
        }
        public List<Khoa> Dsk
        {
            get { return dsk; }
            set { dsk = value; }
        }

        public List<Lop> Dsl
        {
            get { return dsl; }
            set { dsl = value; }
        }
        public void LuuFile()
        {
            FileStream fs = new FileStream(Application.StartupPath + @"/FILE/file", FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs,frmcha.DSCHA);
            fs.Close();
        }
        public void readfile()
        {
            if (File.Exists(Application.StartupPath + @"/FILE/file") == true)
            {
                FileStream fs = new FileStream(Application.StartupPath + @"/FILE/file", FileMode.Open);
                BinaryFormatter bf = new BinaryFormatter();
                frmcha.DSCHA = (Cha)bf.Deserialize(fs);
                fs.Close();
            }
        }

        
    }
}
