﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testQLkituc.Data
{
    [Serializable]
    public class Khoa
    {
        public String MaKhoa { get; set; }
        public string TenKhoa { get; set; }
        List<Lop> DsLop = new List<Lop>();

        public List<Lop> DsLop1
        {
            get { return DsLop; }
            set { DsLop = value; }
        }
        public Khoa()
        {
            MaKhoa = "";
            TenKhoa = "";

        }
        public Khoa(string Makhoa, string Tenkhoa)
        {
            MaKhoa = Makhoa;
            TenKhoa = Tenkhoa;
        }
    }
}
