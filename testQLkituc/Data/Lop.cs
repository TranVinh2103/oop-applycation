﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testQLkituc.Data
{
    [Serializable]
    public class Lop
    {
        List<SinhVien> DsSinhVien = new List<SinhVien>();

        public List<SinhVien> DsSinhVien1
        {
            get { return DsSinhVien; }
            set { DsSinhVien = value; }
        }
        public string Malop { get; set; }
        public string TenLop { get; set; }
        public string NienKhoa { get; set;}
        public string GVCN { get; set; }
        public string SDT { get; set; }
        public Khoa Khoa1  {get;set;}

        public Lop()
        {
            Malop = "";
            TenLop = "";
            NienKhoa = "";
            GVCN = "";
            SDT = "";
        
           
        }
        public Lop(string malop, string tenlop, string nienkhoa, string gvcn, string sdt)
        {
            Malop = malop;
            TenLop = tenlop;
            NienKhoa = nienkhoa;
            GVCN = gvcn;
            SDT = sdt;
            
        }
    }
}
