﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testQLkituc.Data
{
    [Serializable]
    public class SinhVien
    {
        List<HopDong> dsHopDong=new List<HopDong>();

        public List<HopDong> DsHopDong
        {
          get { return dsHopDong; }
          set { dsHopDong = value; }
        }
        private string masv;
        private string hoten;
        private DateTime ngaysinh = new DateTime();
        private bool gioitinh;
        private string quequan;
        private string sodt;
        private string email;
        private Lop lop;

        public Lop Lop
        {
            get { return lop; }
            set { lop = value; }
        }
        public SinhVien()
        {
            masv = "";
            hoten = "";
            ngaysinh = DateTime.Today;
            gioitinh = false;
            quequan = "";
            sodt = "";
            email = "";
        }
        public SinhVien(string ma, string ht, DateTime ns, bool gt, string qq, string sdt, string eml)
        {
            masv = ma;
            hoten = ht;
            ngaysinh = ns;
            gioitinh = gt;
            quequan = qq;
            sodt = sdt;
            email = eml;
        }
        public string Masv
        {
            get { return masv; }
            set { masv = value; }
        }


        public string Hoten
        {
            get { return hoten; }
            set { hoten = value; }
        }


        public DateTime Ngaysinh
        {
            get { return ngaysinh; }
            set { ngaysinh = value; }
        }


        public bool Gioitinh
        {
            get { return gioitinh; }
            set { gioitinh = value; }
        }


        public string Quequan
        {
            get { return quequan; }
            set { quequan = value; }
        }


        public string Sodt
        {
            get { return sodt; }
            set { sodt = value; }
        }


        public string Email
        {
            get { return email; }
            set { email = value; }
        }
    }
}
