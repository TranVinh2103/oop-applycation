﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using testQLkituc.Data;
using testQLkituc.Control;

namespace testQLkituc
{
    public partial class frmHopDong : Form
    {
        CtrlHopDong ctrlhd = new CtrlHopDong();
        CtrlSinhVien ctrlsv = new CtrlSinhVien();

        public frmHopDong()
        {
            InitializeComponent();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (int a in lvwHopDong.SelectedIndices)
            {
                ShowHD(a);

            }
        }
        public void ShowHD(int a)
        {
            HopDong hd = new HopDong();
            hd = frmcha.DSCHA.Dshd[a];
            txtMaHd.Text = hd.Mahd;
            dtpNgaybd.Value = hd.Ngaybd;
            dtpNgaykt.Value = hd.Ngaykt;
            txtTinhTrang.Text = hd.Tinhtrang;
            cbxSinhVien.Text = ctrlsv.Timsv(hd.Sinhvien.Masv).Masv;

        }
        public void ShowListView()
        {
            lvwHopDong.Items.Clear();
            foreach (HopDong hd in frmcha.DSCHA.Dshd)
            {
                lvwHopDong.Items.Add(hd.Mahd);
                int j = lvwHopDong.Items.Count - 1;
                lvwHopDong.Items[j].SubItems.Add(hd.Ngaybd.ToShortDateString());
                lvwHopDong.Items[j].SubItems.Add(hd.Ngaykt.ToShortDateString());
                lvwHopDong.Items[j].SubItems.Add(hd.Tinhtrang);
                lvwHopDong.Items[j].SubItems.Add(hd.Sinhvien.Masv);
            }
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            HopDong hd = new HopDong();
            hd.Mahd = txtMaHd.Text;
            hd.Ngaybd = dtpNgaybd.Value;
            hd.Ngaykt = dtpNgaykt.Value;
            hd.Tinhtrang = txtTinhTrang.Text;
            SinhVien sv = ctrlsv.Timsv(cbxSinhVien.Text);
            sv.DsHopDong = new List<HopDong>();
            hd.Sinhvien = sv;

            if (ctrlhd.themhd(hd) == true)
            {
                ShowListView();
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            HopDong hd = new HopDong();
            hd.Mahd = txtMaHd.Text;
            hd.Ngaybd = dtpNgaybd.Value;
            hd.Ngaykt = dtpNgaykt.Value;
            hd.Tinhtrang = txtTinhTrang.Text;
           
            if (!ctrlhd.suahd(hd))
                MessageBox.Show("Không sửa dữ liệu được!");
            else
            {
                ShowListView();
            }  
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            int flag = 0;
            foreach (int item in lvwHopDong.SelectedIndices)
            {
                if (flag == 0)
                {
                    ctrlhd.Xoahd(item);

                }
                else
                    ctrlhd.Xoahd(item - flag);
                flag++;
            }
            ShowListView();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmHopDong_Load(object sender, EventArgs e)
        {
            ShowListView();
            foreach(SinhVien sv in frmcha.DSCHA.Dssv)
            {
                cbxSinhVien.Items.Add(sv.Masv);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }
    }
}
