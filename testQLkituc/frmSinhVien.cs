﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using testQLkituc.Data;
using testQLkituc.Control;

namespace testQLkituc
{
    public partial class frmSinhVien : Form
    {
        public CtrlSinhVien ctrlsv = new CtrlSinhVien();
        CtrlLop ctrllop = new CtrlLop();
        public frmSinhVien()
        {
            InitializeComponent();
        }
        public void showsv(int a)
        {
            SinhVien sv = new SinhVien();
            sv = frmcha.DSCHA.Dssv[a];
            txtMasv.Text = sv.Masv;
            txtHoten.Text = sv.Hoten;
            dtpNgaysinh.Value = sv.Ngaysinh;
            chkGioitinh.Checked = sv.Gioitinh;
            txtQueQuan.Text = sv.Quequan;
            txtSoDT.Text = sv.Sodt;
            txtEmail.Text = sv.Email;
            if (sv.Lop != null)
                cbxLop.Text = sv.Lop.Malop;

        }
        public void ShowListView()
        {
            lvwSinhVien.Items.Clear();
            foreach (SinhVien sv in frmcha.DSCHA.Dssv)
            {
                lvwSinhVien.Items.Add(sv.Masv);
                int j = lvwSinhVien.Items.Count - 1;
                lvwSinhVien.Items[j].SubItems.Add(sv.Hoten);
                lvwSinhVien.Items[j].SubItems.Add(sv.Ngaysinh.ToShortDateString());
                if (sv.Gioitinh == true)
                    lvwSinhVien.Items[j].SubItems.Add("Nam");
                else
                    lvwSinhVien.Items[j].SubItems.Add("Nữ");
                lvwSinhVien.Items[j].SubItems.Add(sv.Quequan);
                lvwSinhVien.Items[j].SubItems.Add(sv.Sodt);
                lvwSinhVien.Items[j].SubItems.Add(sv.Email);
                if (sv.Lop != null)
                    lvwSinhVien.Items[j].SubItems.Add(sv.Lop.TenLop);

            }
        }
       
        private void btnXoa_Click(object sender, EventArgs e)
        {

        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            SinhVien a = new SinhVien();
            a.Masv = txtMasv.Text;
            a.Hoten = txtHoten.Text;
            a.Ngaysinh = dtpNgaysinh.Value;
            a.Gioitinh = chkGioitinh.Checked;
            a.Quequan = txtQueQuan.Text;
            a.Sodt = txtSoDT.Text;
            a.Email = txtEmail.Text;
            Lop l = ctrllop.Timl(cbxLop.Text);
            l.DsSinhVien1 = new List<SinhVien>();
            a.Lop = l;
            if (ctrlsv.themsv(a) == true)
            {
                ShowListView();
            }
        }

        private void btnXoa_Click_1(object sender, EventArgs e)
        {
            int flag = 0;
            foreach (int item in lvwSinhVien.SelectedIndices)
            {
                if (flag == 0)
                {
                    ctrlsv.Xoasv(item);

                }
                else
                    ctrlsv.Xoasv(item - flag);
                flag++;
            }
            ShowListView();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            SinhVien sv = new SinhVien();
            sv.Masv = txtMasv.Text;
            sv.Hoten = txtHoten.Text;
            sv.Ngaysinh = dtpNgaysinh.Value;
           sv.Gioitinh = chkGioitinh.Checked;
            sv.Quequan = txtQueQuan.Text;
            sv.Sodt = txtSoDT.Text;
            sv.Email = txtEmail.Text;
            if (!ctrlsv.suasv(sv))
                MessageBox.Show("Không sửa dữ liệu được!");
            else
            {
                ShowListView();
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lvwSinhVien_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (int a in lvwSinhVien.SelectedIndices)
            {
                showsv(a);

            }
        }

        private void frmSinhVien_Load(object sender, EventArgs e)
        {
            ShowListView();
            foreach(Lop l in frmcha.DSCHA.Dsl)
            {
                cbxLop.Items.Add(l.Malop);
            }
        }

        private void chkGioitinh_CheckedChanged_1(object sender, EventArgs e)
        {
            if (chkGioitinh.Checked == false)
            {
                chkGioitinh.Text = "Nữ";
            }
            else
            {
                chkGioitinh.Text = "Nam";
            }
        }
    }
}
