﻿namespace testQLkituc
{
    partial class frmChuyenPhong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lvwAllSinhVien = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.cbxMaPhong = new System.Windows.Forms.ComboBox();
            this.lvwSvPhong = new System.Windows.Forms.ListView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lvwAllSinhVien);
            this.panel1.Location = new System.Drawing.Point(310, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(414, 431);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cbxMaPhong);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(292, 56);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lvwSvPhong);
            this.panel3.Location = new System.Drawing.Point(12, 74);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(292, 369);
            this.panel3.TabIndex = 2;
            // 
            // lvwAllSinhVien
            // 
            this.lvwAllSinhVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwAllSinhVien.Location = new System.Drawing.Point(0, 0);
            this.lvwAllSinhVien.Name = "lvwAllSinhVien";
            this.lvwAllSinhVien.Size = new System.Drawing.Size(414, 431);
            this.lvwAllSinhVien.TabIndex = 0;
            this.lvwAllSinhVien.UseCompatibleStateImageBehavior = false;
            this.lvwAllSinhVien.SelectedIndexChanged += new System.EventHandler(this.lvwAllSinhVien_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã Phòng:";
            // 
            // cbxMaPhong
            // 
            this.cbxMaPhong.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxMaPhong.FormattingEnabled = true;
            this.cbxMaPhong.Location = new System.Drawing.Point(107, 18);
            this.cbxMaPhong.Name = "cbxMaPhong";
            this.cbxMaPhong.Size = new System.Drawing.Size(121, 28);
            this.cbxMaPhong.TabIndex = 1;
            // 
            // lvwSvPhong
            // 
            this.lvwSvPhong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwSvPhong.Location = new System.Drawing.Point(0, 0);
            this.lvwSvPhong.Name = "lvwSvPhong";
            this.lvwSvPhong.Size = new System.Drawing.Size(292, 369);
            this.lvwSvPhong.TabIndex = 0;
            this.lvwSvPhong.UseCompatibleStateImageBehavior = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button2);
            this.panel4.Controls.Add(this.button1);
            this.panel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(12, 449);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(712, 41);
            this.panel4.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(481, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(201, 35);
            this.button1.TabIndex = 0;
            this.button1.Text = "Chuyển Vào Phòng";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(26, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(202, 35);
            this.button2.TabIndex = 1;
            this.button2.Text = "Kích Khỏi Phòng Hiện Tại";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // frmChuyenPhong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 523);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "frmChuyenPhong";
            this.Text = "frmChuyenPhong";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView lvwAllSinhVien;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cbxMaPhong;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListView lvwSvPhong;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}