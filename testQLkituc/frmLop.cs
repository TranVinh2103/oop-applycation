﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using testQLkituc.Data;
using testQLkituc.Control;

namespace testQLkituc
{
    public partial class frmLop : Form
    {
        CtrlLop ctrll = new CtrlLop();
        CtrlKhoa ctrlk = new CtrlKhoa();
        public frmLop()
        {
            InitializeComponent();
        }
        public void showlop(int a)
        {
            Lop l = new Lop();
            l = frmcha.DSCHA.Dsl[a];
            txtMalop.Text = l.Malop;
            txtNienKhoa.Text = l.NienKhoa;
            txtTenLop.Text = l.TenLop;
            txtGVCN.Text = l.GVCN;
            txtSDT.Text = l.SDT;
            cboKhoa.Text = ctrlk.Timk(l.Khoa1.MaKhoa).MaKhoa;
           
           

        }
        public void ShowListView()
        {
            lvwLop.Items.Clear();
           
            foreach (Lop l in frmcha.DSCHA.Dsl)
            {
                lvwLop.Items.Add(l.Malop);
                int j = lvwLop.Items.Count - 1;
                lvwLop.Items[j].SubItems.Add(l.TenLop);
                lvwLop.Items[j].SubItems.Add(l.NienKhoa);
                lvwLop.Items[j].SubItems.Add(l.GVCN);
                lvwLop.Items[j].SubItems.Add(l.SDT);
                if(l.Khoa1!=null)
                lvwLop.Items[j].SubItems.Add(l.Khoa1.TenKhoa);
               
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            Lop l = new Lop();
            l.Malop = txtMalop.Text;
            l.TenLop = txtTenLop.Text;
            l.NienKhoa = txtNienKhoa.Text;
            l.GVCN = txtGVCN.Text;
            l.SDT = txtSDT.Text;
            Khoa k = ctrlk.Timk(cboKhoa.Text);
            k.DsLop1 = new List<Lop>();
            l.Khoa1 = k;
           
                if (ctrll.theml(l) == true)
                {
                    ShowListView();
                }
            
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            Lop l = new Lop();
            l.Malop = txtMalop.Text;
            l.TenLop = txtTenLop.Text;
            l.NienKhoa = txtNienKhoa.Text;
            l.GVCN = txtGVCN.Text;
            l.SDT = txtSDT.Text;
            Khoa k = ctrlk.Timk(cboKhoa.Text);
            k.DsLop1 = new List<Lop>();
            l.Khoa1 = k;
            if (!ctrll.sual(l))
                MessageBox.Show("Không sửa dữ liệu được!");
            else
            {
                ShowListView();
            }  
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            int flag = 0;
            foreach (int item in lvwLop.SelectedIndices)
            {
                if (flag == 0)
                {
                    ctrll.Xoal(item);

                }
                else
                    ctrll.Xoal(item - flag);
                flag++;
            }
            ShowListView();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lvwLop_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (int a in lvwLop.SelectedIndices)
            {
                showlop(a);

            }
        }

        private void frmLop_Load(object sender, EventArgs e)
        {
            ShowListView();
            Lop l=new Lop();
            foreach(Khoa k in frmcha.DSCHA.Dsk )
            {
                cboKhoa.Items.Add(k.MaKhoa);
               
            }
            
        }

        private void btnkhoa_Click(object sender, EventArgs e)
        {
          
            //foreach (Khoa k in frmcha.DSCHA.Dsk)
            //{
            //    cboKhoa.Items.Add(k.TenKhoa);

            //}
            
        }

        private void cboKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
