﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using testQLkituc.Control;
using testQLkituc.Data;

namespace testQLkituc
{
    public partial class frmPhong : Form
    {
        CtrlPhong ctrlp = new CtrlPhong(); 
        public frmPhong()
        {
            InitializeComponent();
        }
        public void ShowP(int a)
        {
            Phong p = new Phong();
            p = frmcha.DSCHA.Dsp[a];
            txtMaPhong.Text = p.MaPhong;
            txtTenPhong.Text = p.TenPhong;
            chkLoaiP.Checked = p.LoaiPhong;
            txtDienTich.Text = p.DienTich.ToString();
        }

        public void ShowListView()
        {
            lvwDSPhong.Items.Clear();
            foreach (Phong p in frmcha.DSCHA.Dsp)
            {
                lvwDSPhong.Items.Add(p.MaPhong);
                int j = lvwDSPhong.Items.Count - 1;
                lvwDSPhong.Items[j].SubItems.Add(p.TenPhong);
                if (chkLoaiP.Checked == true)
                {
                    lvwDSPhong.Items[j].SubItems.Add("Nam");
                }
                else
                {
                    lvwDSPhong.Items[j].SubItems.Add("Nữ");
                }
                lvwDSPhong.Items[j].SubItems.Add(p.DienTich.ToString());
                lvwDSPhong.Items[j].SubItems.Add(p.TinhTrang);
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            Phong p = new Phong();
            p.MaPhong = txtMaPhong.Text;
            p.TenPhong = txtTenPhong.Text;
            p.LoaiPhong = chkLoaiP.Checked;
            p.DienTich = float.Parse(txtDienTich.Text);
            p.TinhTrang = txtTinhTrang.Text;
            if (ctrlp.them(p) == true)
            {
                ShowListView();
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            Phong p = new Phong();
            p.MaPhong = txtMaPhong.Text;
            p.TenPhong = txtTenPhong.Text;
            p.LoaiPhong = chkLoaiP.Checked;
            p.DienTich = float.Parse(txtDienTich.Text);
            p.TinhTrang = txtTinhTrang.Text;
            if (!ctrlp.sua(p))
                MessageBox.Show("Không sửa dữ liệu được!");
            else
            {
                ShowListView();
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            int flag = 0;
            foreach (int item in lvwDSPhong.SelectedIndices)
            {
                if (flag == 0)
                {
                    ctrlp.Xoap(item);

                }
                else
                    ctrlp.Xoap(item - flag);
                flag++;
            }
            ShowListView();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void chkLoaiP_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLoaiP.Checked == true)
            {
                chkLoaiP.Text = "Nam";
            }
            else
                chkLoaiP.Text = "Nữ";
        }

        private void lvwDSPhong_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (int a in lvwDSPhong.SelectedIndices)
            {
                ShowP(a);

            }
        }

        private void frmPhong_Load(object sender, EventArgs e)
        {
            ShowListView();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void txtTinhTrang_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDienTich_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTenPhong_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMaPhong_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
