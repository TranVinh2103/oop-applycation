﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testQLkituc.Data;
using testQLkituc.Control;
using System.Windows.Forms;

namespace testQLkituc.Control
{
   
    public class CtrlKhoa
    {
        public static Cha ch;

        public static Cha CH
        {
          get { return CtrlKhoa.ch; }
          set { CtrlKhoa.ch = value; }
        }
        
        public CtrlKhoa()
        {
            ch = frmcha.DSCHA;
        }

       

        public bool themk(Khoa k)
        {
            try
            {
                if (Timk(k.MaKhoa)==null)
                {
                   CH.Dsk.Add(k);
                }
              
                else
                    MessageBox.Show("khong the them!!");
                    
        
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool suak(Khoa t)
        {
            try
            {
                if(Timk(t.MaKhoa)!=null)
                {
                    Khoa k = Timk(t.MaKhoa);

                    k.TenKhoa =t.TenKhoa;
                  
                    return true;

                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        public Khoa Timk(string makhoa)
        {
           
            foreach(Khoa k in CH.Dsk)
            {
                if (k.MaKhoa == makhoa)
                    return k;
            }
            return null;
        }
        public Khoa TimTenK(string makhoa)
        {

            foreach (Khoa k in CH.Dsk)
            {
                if (k.TenKhoa == makhoa)
                    return k;
            }
            return null;
        }
        public void Xoak(int a)
        {
            CH.Dsk.RemoveAt(a);
        }
    }
}
