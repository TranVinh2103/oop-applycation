﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testQLkituc.Data;
using System.Windows.Forms;

namespace testQLkituc.Control
{
    public class CtrlHopDong
    {
       
        public static Cha ch;
        // private List<hopdong> dshd;
        public static Cha CH
        {
            get { return CtrlHopDong.ch; }
            set { CtrlHopDong.ch = value; }
        }
         public CtrlHopDong()
        {
            ch = frmcha.DSCHA;
        }
         
         public bool themhd(HopDong hd)
         {
             try
             {
                 if (Timhd(hd.Mahd)==null)
                 {
                     hd.Sinhvien.DsHopDong.Add(hd);
                     CH.Dshd.Add(hd);
                 }
                 else
                     MessageBox.Show("khong the them!!");
                 return true;
             }
             catch
             {
                 MessageBox.Show("loi roi", "canh bao", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 return false;
             }
         }
         public bool suahd(HopDong a)
         {
             try
             {

                 if (Timhd(a.Mahd)!=null)
                 {
                     HopDong hd = Timhd(a.Mahd);
                     hd.Ngaybd = a.Ngaybd;
                     hd.Ngaykt = a.Ngaykt;
                     hd.Tinhtrang = a.Tinhtrang;
                     return true;
                 }
                  
                 return false;
             }
             catch
             {
                 return false;
             }
         }
         public HopDong Timhd(string mahd)
         {
             foreach(HopDong hd in CH.Dshd)
             {
                 if (hd.Mahd == mahd)
                     return hd;
             }
             return null;
         }
         public void Xoahd(int a)
         {
             CH.Dshd.RemoveAt(a);
         }

    }
}
