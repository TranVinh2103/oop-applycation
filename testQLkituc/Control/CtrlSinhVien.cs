﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testQLkituc.Data;
using testQLkituc.Control;
using System.Windows.Forms;

namespace testQLkituc.Control
{
    public class CtrlSinhVien
    {
        public static Cha ch;

        public static Cha CH
        {
            get { return CtrlSinhVien.ch; }
            set { CtrlSinhVien.ch = value; }
        }
         public CtrlSinhVien()
        {
            ch = frmcha.DSCHA;
        }
        public bool themsv(SinhVien sv)
        {
           
                if (Timsv(sv.Masv)==null)
                {
                    sv.Lop.DsSinhVien1.Add(sv);
                    CH.Dssv.Add(sv);
                }
                else
                    MessageBox.Show("khong the them!!!");
                return true;
            
        }

        public bool suasv(SinhVien sv)
        {
            try
            {
                
                    if (Timsv(sv.Masv)!=null)
                    {
                        SinhVien a=Timsv(sv.Masv);
                        a.Hoten = sv.Hoten;
                        a.Ngaysinh = sv.Ngaysinh;
                        a.Gioitinh = sv.Gioitinh;
                        a.Quequan = sv.Quequan;
                        a.Sodt = sv.Sodt;
                        a.Email = sv.Email;
                        return true;
                    }        
                        return false;
            }
            catch
            {
                return false;
            }
        }
        public SinhVien Timsv(string masv)
        {
           foreach(SinhVien sv in CH.Dssv)
           {
               if (sv.Masv == masv)
                   return sv;
           }
           return null;
        }
        public void Xoasv(int a)
        {
            CH.Dssv.RemoveAt(a);
        }
    }
}
