﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testQLkituc.Control;
using testQLkituc.Data;
using System.Windows.Forms;

namespace testQLkituc.Control
{
    class CtrlLop
    {
        public static Cha ch;

        public static Cha CH
        {
          get { return CtrlLop.ch; }
          set { CtrlLop.ch = value; }
        }
        
        public CtrlLop()
        {
            ch = frmcha.DSCHA;
        }

       

        public bool theml(Lop l)
        {
            
                if (Timl(l.Malop)==null)
                {
                    l.Khoa1.DsLop1.Add(l);
                    CH.Dsl.Add(l);
                    return true;
                }
                //else
                //    MessageBox.Show("khong the them lop!!!");

                return true;
           
        }
      
       public bool sual(Lop p)
        {
            try
            {
               
                   if(Timl(p.Malop)!=null)
                   {
                       Lop l = Timl(p.Malop);
                       l.Khoa1.DsLop1.Remove(p);
                       l.TenLop = p.TenLop;
                       l.NienKhoa = p.NienKhoa;
                       l.GVCN = p.GVCN;
                       l.SDT = p.SDT;
                       l.Khoa1 = p.Khoa1;
                       return true;
                   }
                   return false;
            }
            catch
            {
                return false;
            }
        }
        public Lop Timl(string malop)
        {
          
            foreach(Lop l in CH.Dsl)
            {
                if (l.Malop == malop)
                    return l;
            }
            return null;
        }
        public void Xoal(int a)
        {
            foreach(SinhVien sv in CH.Dssv)
            {
                if (sv.Lop.Malop == CH.Dsl[a].Malop)
                    sv.Lop = null;
                break;
            }
            CH.Dsl.RemoveAt(a);
        }
    }
}
