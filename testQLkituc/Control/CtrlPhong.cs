﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testQLkituc.Data;
using testQLkituc.Control;
using System.Windows.Forms;

namespace testQLkituc.Control
{
    public class CtrlPhong
    {
         public static Cha ch;

        public static Cha CH
        {
          get { return CtrlPhong.ch; }
          set { CtrlPhong.ch = value; }
        }
        
        public CtrlPhong()
        {
            ch = frmcha.DSCHA;
        }

       

        public bool them(Phong p)
        {
            try
            {
                if (Timp(p.MaPhong) < 0)
                {
                    CH.Dsp.Add(p);
                }
                else
                    MessageBox.Show("khong the them!!");
                return true;
            }
            catch
            {
                return false;
            }
        }
      
       public bool sua(Phong p)
        {
            try
            {
                foreach (Phong a in CH.Dsp)
                {
                    if(a.MaPhong == p.MaPhong)
                    a.TenPhong =p.TenPhong;
                    a.LoaiPhong = p.LoaiPhong;
                    a.TinhTrang = p.TinhTrang;
                    a.DienTich = p.DienTich;
                   

                    return true;

                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        public int Timp(string map)
        {
            int a = -1;
            for(int i = 0;i<CH.Dsp.Count ;i++)
            {
                if (map == CH.Dsp[i].MaPhong)
                    a = i;
            }
            return a;
        }
        public void Xoap(int a)
        {
            CH.Dsp.RemoveAt(a);
        }
    
    }
}
