﻿namespace testQLkituc
{
    partial class frmcha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileSinhVien = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileHopDong = new System.Windows.Forms.ToolStripMenuItem();
            this.qLPhongToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qLKhoaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qLLơpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lưuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile,
            this.lưuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(631, 36);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mnuFile
            // 
            this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFileSinhVien,
            this.mnuFileHopDong,
            this.qLPhongToolStripMenuItem,
            this.qLKhoaToolStripMenuItem,
            this.qLLơpToolStripMenuItem});
            this.mnuFile.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Size = new System.Drawing.Size(121, 32);
            this.mnuFile.Text = "Chức Năng";
            // 
            // mnuFileSinhVien
            // 
            this.mnuFileSinhVien.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.mnuFileSinhVien.Name = "mnuFileSinhVien";
            this.mnuFileSinhVien.Size = new System.Drawing.Size(205, 32);
            this.mnuFileSinhVien.Text = "QL Sinh Viên";
            this.mnuFileSinhVien.Click += new System.EventHandler(this.mnuFileSinhVien_Click);
            // 
            // mnuFileHopDong
            // 
            this.mnuFileHopDong.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.mnuFileHopDong.Name = "mnuFileHopDong";
            this.mnuFileHopDong.Size = new System.Drawing.Size(205, 32);
            this.mnuFileHopDong.Text = "QL Hợp Đồng";
            this.mnuFileHopDong.Click += new System.EventHandler(this.mnuFileHopDong_Click);
            // 
            // qLPhongToolStripMenuItem
            // 
            this.qLPhongToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.qLPhongToolStripMenuItem.Name = "qLPhongToolStripMenuItem";
            this.qLPhongToolStripMenuItem.Size = new System.Drawing.Size(205, 32);
            this.qLPhongToolStripMenuItem.Text = "QL Phòng";
            this.qLPhongToolStripMenuItem.Click += new System.EventHandler(this.qLPhongToolStripMenuItem_Click);
            // 
            // qLKhoaToolStripMenuItem
            // 
            this.qLKhoaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.qLKhoaToolStripMenuItem.Name = "qLKhoaToolStripMenuItem";
            this.qLKhoaToolStripMenuItem.Size = new System.Drawing.Size(205, 32);
            this.qLKhoaToolStripMenuItem.Text = "QL Khoa";
            this.qLKhoaToolStripMenuItem.Click += new System.EventHandler(this.qLKhoaToolStripMenuItem_Click);
            // 
            // qLLơpToolStripMenuItem
            // 
            this.qLLơpToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.qLLơpToolStripMenuItem.Name = "qLLơpToolStripMenuItem";
            this.qLLơpToolStripMenuItem.Size = new System.Drawing.Size(205, 32);
            this.qLLơpToolStripMenuItem.Text = "QL Lớp";
            this.qLLơpToolStripMenuItem.Click += new System.EventHandler(this.qLLơpToolStripMenuItem_Click);
            // 
            // lưuToolStripMenuItem
            // 
            this.lưuToolStripMenuItem.Name = "lưuToolStripMenuItem";
            this.lưuToolStripMenuItem.Size = new System.Drawing.Size(56, 32);
            this.lưuToolStripMenuItem.Text = "Lưu";
            this.lưuToolStripMenuItem.Click += new System.EventHandler(this.lưuToolStripMenuItem_Click);
            // 
            // frmcha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 336);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmcha";
            this.Text = "Quản Lý Kí Túc Xá";
            this.Load += new System.EventHandler(this.frmcha_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuFileHopDong;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSinhVien;
        private System.Windows.Forms.ToolStripMenuItem qLPhongToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qLKhoaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qLLơpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lưuToolStripMenuItem;
    }
}